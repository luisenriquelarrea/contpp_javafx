package com.contpp.controller;

import com.contpp.classes.AlertHandler;
import com.contpp.classes.Nomina;
import com.contpp.classes.Validate;
import com.contpp.model.SueldosSalarios;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.CheckBox;

public class PrimaryController {
    @FXML private TextField txtEjercicio;
    @FXML private TextField txtSDinicio;
    @FXML private TextField txtSDfinal;
    @FXML private TextField txtDiasVacaciones;
    @FXML private TextField txtStep;
    @FXML private ChoiceBox<String> choicePeriodicidad;
    @FXML private CheckBox checkBonos;

    DecimalFormat decimalFormat;

    AlertHandler alertHandler;
    Validate validate;

    public PrimaryController(){
        txtEjercicio = new TextField();
        txtSDinicio = new TextField();
        txtSDfinal = new TextField();
        txtDiasVacaciones = new TextField();
        txtStep = new TextField();
        choicePeriodicidad = new ChoiceBox<>();
        checkBonos = new CheckBox();

        decimalFormat = new DecimalFormat();

        alertHandler = new AlertHandler();
        validate = new Validate();
    }

    private void calculaValores(HashMap<String, String> valores){
        int ejercicio = Integer.parseInt(valores.get("ejercicio"));
        int diasPagados = Integer.parseInt(valores.get("diasPagados"));
        int diasVacaciones = Integer.parseInt(valores.get("diasVacaciones"));
        float sdInicio = Float.parseFloat(valores.get("sdInicio"));
        float sdFinal = Float.parseFloat(valores.get("sdFinal"));
        float step = Float.parseFloat(valores.get("step"));
        String fechaPago = valores.get("fechaPago");
        decimalFormat.setMaximumFractionDigits(2);
        float salarioDiario = sdInicio;
        final int diasAguinaldo = 15;
        
        float salarioDiarioIntegrado = 0f;
        float sueldos = 0f;
        float percepcionMensual = 0f;
        float isrDeterminado = 0f;
        float isrPagar = 0f;
        float subsidioEmpleoCausado = 0f;
        float subsidioEmpleoEntregado = 0f;
        float imss = 0f;
        float neto = 0f;

        Nomina nomina = new Nomina();

        while(salarioDiario <= sdFinal){
            salarioDiarioIntegrado = nomina.salarioDiarioIntegrado(salarioDiario, diasVacaciones);
            sueldos = nomina.sueldosSalarios(salarioDiario, diasPagados);
            percepcionMensual = nomina.percepcionGravadaMensual(sueldos, diasPagados);
            isrDeterminado = nomina.isrDeterminado(sueldos, fechaPago, diasPagados);
            subsidioEmpleoCausado = nomina.subsidioEmpleoCausado(percepcionMensual, fechaPago);
            subsidioEmpleoEntregado = nomina.subsidioEmpleoEntregado(subsidioEmpleoCausado, diasPagados);
            isrPagar = nomina.isrPagar(isrDeterminado);
            imss = nomina.imss(salarioDiario, diasPagados, diasAguinaldo, diasVacaciones, fechaPago);
            neto = nomina.neto(sueldos, subsidioEmpleoEntregado, isrPagar, imss);

            SueldosSalarios sueldosSalarios = new SueldosSalarios();
            sueldosSalarios.setEjercicio(ejercicio);
            sueldosSalarios.setDiasPagados(diasPagados);
            sueldosSalarios.setDiasVacaciones(diasVacaciones);
            sueldosSalarios.setSalarioDiario(salarioDiario);
            sueldosSalarios.setSalarioDiarioIntegrado(salarioDiarioIntegrado);
            sueldosSalarios.setSueldosSalarios(sueldos);
            sueldosSalarios.setSubsidioEmpleoCausado(subsidioEmpleoCausado);
            sueldosSalarios.setSubsidioEmpleoEntregado(subsidioEmpleoEntregado);
            sueldosSalarios.setIsrDeterminado(isrDeterminado);
            sueldosSalarios.setIsrPagar(isrPagar);
            sueldosSalarios.setIMSS(imss);
            sueldosSalarios.setNeto(neto);
            sueldosSalarios.save();

            salarioDiario = Float.parseFloat(decimalFormat.format(salarioDiario += step));
        }
        alertHandler.setAlertType("information");
        alertHandler.setTitle("...::: contpp JavaFX :::...");
        alertHandler.setHeader("OK");
        alertHandler.setContent("");
        alertHandler.message();
        return;
    }

    private int getDiasPeriodicidad(String periodicidad){
        if(periodicidad.equals("S"))
            return 7;
        if(periodicidad.equals("Q"))
            return 15;
        return 30;
    }

    @FXML
    private void calcularSueldosSalarios() throws IOException {
        System.out.println("Pressed Guardar.");
        String title = "...::: contpp JavaFX :::...";
        int ejercicio;
        try{
            ejercicio = Integer.parseInt(txtEjercicio.getText());
        }catch(NumberFormatException ex){
            alertHandler.setAlertType("warning");
            alertHandler.setTitle(title);
            alertHandler.setHeader("Ingresa un ejercicio valido.");
            alertHandler.setContent("");
            alertHandler.message();
            return;
        }
        float sdInicio;
        try{
            sdInicio = Float.parseFloat(txtSDinicio.getText());
        }catch(NumberFormatException ex){
            alertHandler.setAlertType("warning");
            alertHandler.setTitle(title);
            alertHandler.setHeader("Ingresa un SD inicio valido.");
            alertHandler.setContent("");
            alertHandler.message();
            return;
        }
        float sdFinal;
        try{
            sdFinal = Float.parseFloat(txtSDfinal.getText());
        }catch(NumberFormatException ex){
            alertHandler.setAlertType("warning");
            alertHandler.setTitle(title);
            alertHandler.setHeader("Ingresa un SD final valido.");
            alertHandler.setContent("");
            alertHandler.message();
            return;
        }
        int diasVacaciones;
        try{
            diasVacaciones = Integer.parseInt(txtDiasVacaciones.getText());
        }catch(NumberFormatException ex){
            alertHandler.setAlertType("warning");
            alertHandler.setTitle(title);
            alertHandler.setHeader("Ingresa un numero dias vacaciones valido.");
            alertHandler.setContent("");
            alertHandler.message();
            return;
        }
        float step;
        try{
            step = Float.parseFloat(txtStep.getText());
        }catch(NumberFormatException ex){
            alertHandler.setAlertType("warning");
            alertHandler.setTitle(title);
            alertHandler.setHeader("Ingresa un step valido.");
            alertHandler.setContent("");
            alertHandler.message();
            return;
        }
        String periodicidad = choicePeriodicidad.getValue().toString();
        int diasPagados = getDiasPeriodicidad(periodicidad);
        boolean hasBonos = checkBonos.isSelected();
        String fechaPago = ejercicio+"-02-01";
        HashMap<String, String> valores = new HashMap<String, String>();
        valores.put("ejercicio", ejercicio+"");
        valores.put("diasPagados", diasPagados+"");
        valores.put("diasVacaciones", diasVacaciones+"");
        valores.put("sdInicio", sdInicio+"");
        valores.put("sdFinal", sdFinal+"");
        valores.put("step", step+"");
        valores.put("hasBonos", Boolean.toString(hasBonos));
        valores.put("fechaPago", fechaPago);
        calculaValores(valores);
    }
}
