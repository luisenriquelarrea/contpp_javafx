package com.contpp.controller;

import java.io.IOException;
import java.net.URL;

import com.contpp.App;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;
import javafx.scene.Node;

public class MainController {
    @FXML
    private BorderPane borderPane;

    @FXML
    private void handleShowView(ActionEvent e) throws IOException{
        String fxml = (String) ((Node)e.getSource()).getUserData();
        loadFXML(App.class.getResource("view/"+fxml+".fxml"));
    }

    private void loadFXML(URL url) {
        try {
            FXMLLoader loader = new FXMLLoader(url);
            borderPane.setCenter(loader.load());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
