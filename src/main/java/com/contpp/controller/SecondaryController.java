package com.contpp.controller;

import java.util.LinkedList;

import com.contpp.classes.AlertHandler;
import com.contpp.classes.Nomina;

import com.contpp.model.SueldosSalarios;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.TableView;

public class SecondaryController {
    @FXML private TextField txtNeto;
    @FXML private TableView<SueldosSalarios> tableView;

    AlertHandler alertHandler;

    public SecondaryController(){
        txtNeto = new TextField();

        alertHandler = new AlertHandler();
    }

    @FXML
    private void consultarSueldosSalarios(){
        String title = "...::: contpp JavaFX :::...";
        float neto;
        try{
            neto = Float.parseFloat(txtNeto.getText());
        }catch(NumberFormatException ex){
            alertHandler.setAlertType("warning");
            alertHandler.setTitle(title);
            alertHandler.setHeader("Ingresa un valor neto valido.");
            alertHandler.setContent("");
            alertHandler.message();
            return;
        }
        Nomina nomina = new Nomina();
        LinkedList<SueldosSalarios> sueldosSalariosList = nomina.getSueldosSalarios(neto);
        ObservableList<SueldosSalarios> data = tableView.getItems();
        data.clear();
        for(SueldosSalarios obj : sueldosSalariosList){
            data.add(obj);
        }
    }
}
