package com.contpp.classes;

import java.util.ArrayList;
import java.util.LinkedList;
import java.sql.ResultSet;

import com.contpp.model.IsrSemanal;
import com.contpp.model.SubsidioEmpleo;
import com.contpp.model.SueldosSalarios;
import com.contpp.model.Uma;

public class Nomina {
    Validate validate;

    public Nomina() {
        validate = new Validate();
    }

    public float salarioDiarioIntegrado(float salarioDiario, int diasVacaciones){
        float factorIntegracion = calculoFactorIntegracion(diasVacaciones);
        return salarioDiario * factorIntegracion;
    }

    public float calculoFactorIntegracion(int diasVacaciones){
        final int diasAnio = 365;
        final int diasAguinaldo = 15;
        final float primaVacacional = 0.25f;
        return (diasAnio + diasVacaciones * primaVacacional + diasAguinaldo) / diasAnio;
    }

    public float imss(float salarioDiario, int diasPagados, int diasAguinaldo, 
                        int diasVacaciones, String fechaPago){
        float sbc = calculoSalarioBaseCotizacion(salarioDiario, diasAguinaldo, diasVacaciones);
        Uma uma = getUma(fechaPago);
        float imss = calculoIMSS(sbc, uma.getMonto(), diasPagados);
        return imss;
    }

    public float calculoIMSS(float sbc, float uma, int diasPagados){
        float imss = 0f;
        float diferencia_3uma_sbc = sbc - (uma * 3);
        if(diferencia_3uma_sbc > 0)
            imss = (float)(0.40 / 100) * diferencia_3uma_sbc * diasPagados;

        //2.2 Gastos médicos para pensionados y beneficiarios
        imss = imss + (float)(0.375 / 100) * sbc * diasPagados;

        //2.3 En dinero
        imss = imss + (float)(0.25 / 100) * sbc * diasPagados;

        //#3 Invalidez y Vida

        //3.1 En especie y dinero
        imss = imss + (float)(0.625 / 100) * sbc * diasPagados;

        //#4 Retiro, Cesantía en Edad Avanzada y Vejez (CEAV)
        imss = imss + (float)(1.125 / 100) * sbc * diasPagados;

        return imss;
    }

    public float calculoSalarioBaseCotizacion(float salarioDiario, int diasAguinaldo, int diasVacaciones){
        final int diasAnio = 365;
        final float primaVacacional = 0.25f;
        float sbc = (
            ((diasAguinaldo * salarioDiario) / diasAnio ) 
            + ((diasVacaciones * salarioDiario * primaVacacional) / diasAnio ) 
            + salarioDiario
        );
        return sbc;
    }

    public float isrDeterminado(float totalGravado, String fechaPago, int diasPagados){
        float isrDeterminado = 0f;
        if(diasPagados == 7){
            IsrSemanal isrSemanal = getIsrSemanal(totalGravado, fechaPago);
            isrDeterminado = calculoIsrDeterminado(totalGravado, 
                isrSemanal.getLimiteInferior(),
                isrSemanal.getCuotaFija(),
                isrSemanal.getPorcentajeExcedente()
            );
        }
        return isrDeterminado;
    }

    public float isrPagar(float isrDeterminado){
        return isrDeterminado;
    }

    public float neto(float sueldosSalarios, float subsidioEmpleoEntregado, float isrPagar, float imss){
        return sueldosSalarios + subsidioEmpleoEntregado - isrPagar - imss;
    }

    public float percepcionGravadaMensual(float totalGravado, int diasPagados){
        int semanas = (int)(30.4 / diasPagados);
        return totalGravado * semanas;
    }

    public float subsidioEmpleoCausado(float totalGravado, String fechaPago){
        float subsidioEmpleoCausado = 0f;
        SubsidioEmpleo subsidioEmpleo = getSubsidioEmpleo(totalGravado, fechaPago);
        subsidioEmpleoCausado = subsidioEmpleo.getCuotaFija();
        return subsidioEmpleoCausado;
    }

    public float subsidioEmpleoEntregado(float subsidioEmpleoCausado, int diasPagados){
        if(subsidioEmpleoCausado > 0.0){
            float propDiario = (float)(subsidioEmpleoCausado / 30.4);
            return propDiario * diasPagados;
        }
        return 0;
    }

    public float calculoIsrDeterminado(float totalGravado, float limiteInferior, 
                                        float cuotaFija, float porcentajeExcedente){
        float excedente = totalGravado - limiteInferior;
        float isrExcedente = excedente * porcentajeExcedente / 100;
        float isrDeterminado = isrExcedente + cuotaFija;
        return isrDeterminado;
    }

    public float sueldosSalarios(float salarioDiario, int diasPagados){
        return salarioDiario * diasPagados;
    }

    public IsrSemanal getIsrSemanal(float totalGravado, String fechaPago){
        String sql = "SELECT id, limite_inferior, limite_superior, cuota_fija, porcentaje_excedente "+
            "FROM isr_semanal "+
            "WHERE ? >= limite_inferior "+
            "AND ? <= limite_superior "+
            "AND ? >= fecha_inicio "+
            "AND ? <= fecha_final ";
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(totalGravado);
        params.add(totalGravado);
        params.add(fechaPago);
        params.add(fechaPago);
        DBHandler dbHandler = new DBHandler();
        dbHandler.prepareStatement(sql, params);
        ResultSet rs = dbHandler.runQuery();
        boolean hasResult = validate.validateResultSet(rs);
        if(hasResult == false){
            System.out.println("isrSemanal is required");
            System.exit(0);
        }
        LinkedList<IsrSemanal> isrSemanalList = IsrSemanal.getIsrSemanal(rs);
        dbHandler.close();
        return (IsrSemanal)isrSemanalList.get(0);
    }

    public SubsidioEmpleo getSubsidioEmpleo(float totalGravado, String fechaPago){
        String sql = "SELECT id, limite_inferior, limite_superior, cuota_fija "+
            "FROM subsidio_empleo "+
            "WHERE ? >= limite_inferior "+
            "AND ? <= limite_superior "+
            "AND ? >= fecha_inicio "+
            "AND ? <= fecha_final ";
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(totalGravado);
        params.add(totalGravado);
        params.add(fechaPago);
        params.add(fechaPago);
        DBHandler dbHandler = new DBHandler();
        dbHandler.prepareStatement(sql, params);
        ResultSet rs = dbHandler.runQuery();
        boolean hasResult = validate.validateResultSet(rs);
        if(hasResult == false){
            System.out.println("subsidioEmpleo is required");
            System.exit(0);
        }
        LinkedList<SubsidioEmpleo> subsidioEmpleoList = SubsidioEmpleo.getSubsidioEmpleo(rs);
        dbHandler.close();
        return (SubsidioEmpleo)subsidioEmpleoList.get(0);
    }

    public LinkedList<SueldosSalarios> getSueldosSalarios(float neto){
        String sql = "SELECT "+
        "id, "+
        "ejercicio, "+
        "n_dias_pagados, "+
        "n_dias_vacaciones, "+
        "sd, "+
        "sdi, "+
        "sueldos_salarios, "+
        "subsidio_empleo_causado, "+
        "subsidio_empleo_entregado, "+
        "isr_determinado, "+
        "isr_pagar, "+
        "imss, "+
        "neto "+
        "FROM sueldos_salarios "+
        "ORDER BY ABS(neto - ?) "+
        "LIMIT 2";
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(neto);
        DBHandler dbHandler = new DBHandler();
        dbHandler.prepareStatement(sql, params);
        ResultSet rs = dbHandler.runQuery();
        boolean hasResult = validate.validateResultSet(rs);
        if(hasResult == false){
            System.out.println("SueldosSalarios is required");
            System.exit(0);
        }
        LinkedList<SueldosSalarios> sueldosSalariosList = SueldosSalarios.getSueldosSalarios(rs);
        dbHandler.close();
        return sueldosSalariosList;
    }

    public Uma getUma(String fechaPago){
        String sql = "SELECT id, monto "+
            "FROM uma "+
            "WHERE ? >= fecha_inicio "+
            "AND ? <= fecha_final ";
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(fechaPago);
        params.add(fechaPago);
        DBHandler dbHandler = new DBHandler();
        dbHandler.prepareStatement(sql, params);
        ResultSet rs = dbHandler.runQuery();
        boolean hasResult = validate.validateResultSet(rs);
        if(hasResult == false){
            System.out.println("uma is required");
            System.exit(0);
        }
        LinkedList<Uma> umaList = Uma.getUma(rs);
        dbHandler.close();
        return (Uma)umaList.get(0);
    }
}
