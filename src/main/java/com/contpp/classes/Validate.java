package com.contpp.classes;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Validate {
    public boolean validateResultSet(ResultSet rs){
        boolean hasResult = false;
        try{
            hasResult = rs.next();
            if(hasResult)
                rs.beforeFirst();
        }catch(SQLException e){
            e.printStackTrace();
            System.exit(1);
        }
        return hasResult;
    }
}
