package com.contpp.classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

public class DBHandler{
    private String host;
    private String database;
    private String username;
    private String password;
    private String port;

    private Connection conexion;
    private PreparedStatement pstmt;
    private Statement statement;
    private ResultSet rs;

    public DBHandler(){
        host = "localhost";
        database = "contpp_laravel";
        username = "root";
        password = "12345678";
        port = "3306";
        
        pstmt = null;
        statement = null;
        rs = null;
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");  
            conexion = DriverManager.getConnection("jdbc:mysql://"+host+":"+port+"/"+database, username, password);  
        }catch(Exception e){ 
            e.printStackTrace();
            System.exit(1);
        }
    }

    /*
     * Close MySQL connection.
     */
    public void close(){
        if(pstmt != null){
            try{
                pstmt.close();
            }catch(SQLException e){
                e.printStackTrace();
                System.exit(1);
            }
        }
        if(statement != null){
            try{
                statement.close();
            }catch(SQLException e){
                e.printStackTrace();
                System.exit(1);
            }
        }
        if(rs != null){
            try{
                rs.close();
            }catch(SQLException e){
                e.printStackTrace();
                System.exit(1);
            }
        }
        if(conexion != null){
            try{
                conexion.close();
            }catch(SQLException e){
                e.printStackTrace();
                System.exit(1);
            }
        }
    }

    /*
     * Execute statement into DB.
     *
     * @return int resultado
     */
    public int executeUpdate(){
        int result = 0;
        try{
            result = pstmt.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
            System.exit(1);
        }
        return result;
    }

    public int prepareStatement(String sql, ArrayList<Object> params){
        try{
            pstmt = conexion.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, 
                                                ResultSet.CONCUR_UPDATABLE);
        }catch(SQLException e){
            e.printStackTrace();
            System.exit(1);
        }
        var wrapper = new Object(){
            int i = 1;
        };
        params.forEach((param) -> {
            try{
                pstmt.setString(wrapper.i++, param.toString());
            }catch(SQLException e){
                e.printStackTrace();
                System.exit(1);
            }
            
        });
        return 1;
    }

    /*
     * Execute SELECT statement
     *
     * @return ResultSet rs
     */
    public ResultSet runQuery(){
        try{
            rs = pstmt.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
            System.exit(1);
        }
        return rs;
    }
}  
