package com.contpp.classes;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

public class AlertHandler {
    private String alertType;
    private String title;
    private String header;
    private String content;

    private Alert alert;

    public void setAlertType(String alertType){
        this.alertType = alertType;
    }

    public String getAlertType(){
        return alertType;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return title;
    }

    public void setHeader(String header){
        this.header = header;
    }

    public String getHeader(){
        return header;
    }

    public void setContent(String content){
        this.content = content;
    }

    public String getContent(){
        return content;
    }

    public void message(){
        setAlert();
        alert.setTitle(getTitle());
        alert.setHeaderText(getHeader());
        alert.setContentText(getContent());
        alert.showAndWait().ifPresent(rs -> {
            if (rs == ButtonType.OK) {
                System.out.println("Pressed OK.");
            }
        });
    }

    public void setAlert(){
        switch (getAlertType()){
            case "confirmation":
                alert = new Alert(AlertType.CONFIRMATION);
            break;
            case "error":
                alert = new Alert(AlertType.ERROR);
            break;
            case "warning":
                alert = new Alert(AlertType.WARNING);
            break;
            case "information":
                alert = new Alert(AlertType.INFORMATION);
            break;
            default:
                alert = new Alert(AlertType.NONE);
        }
    }
}
