package com.contpp.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

public class Uma {
    private int id;
    private float monto;

    public int getId(){
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getMonto(){
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public static LinkedList<Uma> getUma(ResultSet rs){
        LinkedList<Uma> list = new LinkedList<Uma>();
        try{
            while(rs.next()) {
                Uma uma = new Uma();
                uma.setId(rs.getInt("id"));
                uma.setMonto(rs.getFloat("monto"));
                list.add(uma);
            }
        }catch(SQLException e){
            e.printStackTrace();
            System.exit(1);
        }
        return list;
    }
}
