package com.contpp.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;

import com.contpp.classes.DBHandler;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleFloatProperty;

public class SueldosSalarios {
    private int id;
    private SimpleIntegerProperty ejercicio = new SimpleIntegerProperty();
    private SimpleIntegerProperty diasPagados = new SimpleIntegerProperty();
    private SimpleIntegerProperty diasVacaciones = new SimpleIntegerProperty();
    private SimpleFloatProperty salarioDiario = new SimpleFloatProperty();
    private SimpleFloatProperty salarioDiarioIntegrado = new SimpleFloatProperty();;
    private SimpleFloatProperty sueldosSalarios = new SimpleFloatProperty();;
    private SimpleFloatProperty subsidioEmpleoCausado = new SimpleFloatProperty();;
    private SimpleFloatProperty subsidioEmpleoEntregado = new SimpleFloatProperty();;
    private SimpleFloatProperty isrDeterminado = new SimpleFloatProperty();;
    private SimpleFloatProperty isrPagar = new SimpleFloatProperty();;
    private SimpleFloatProperty imss = new SimpleFloatProperty();;
    private SimpleFloatProperty neto = new SimpleFloatProperty();;

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public int getEjercicio(){
        return ejercicio.get();
    }

    public void setEjercicio(int ejercicio){
        this.ejercicio.set(ejercicio);
    }

    public int getDiasPagados(){
        return diasPagados.get();
    }

    public void setDiasPagados(int diasPagados){
        this.diasPagados.set(diasPagados);
    }

    public int getDiasVacaciones(){
        return diasVacaciones.get();
    }

    public void setDiasVacaciones(int diasVacaciones){
        this.diasVacaciones.set(diasVacaciones);
    }

    public float getSalarioDiario(){
        return salarioDiario.get();
    }

    public void setSalarioDiario(float salarioDiario){
        this.salarioDiario.set(salarioDiario);
    }

    public float getSalarioDiarioIntegrado(){
        return salarioDiarioIntegrado.get();
    }

    public void setSalarioDiarioIntegrado(float salarioDiarioIntegrado){
        this.salarioDiarioIntegrado.set(salarioDiarioIntegrado);
    }

    public float getSueldosSalarios(){
        return sueldosSalarios.get();
    }

    public void setSueldosSalarios(float sueldosSalarios){
        this.sueldosSalarios.set(sueldosSalarios);
    }

    public float getSubsidioEmpleoCausado(){
        return subsidioEmpleoCausado.get();
    }

    public void setSubsidioEmpleoCausado(float subsidioEmpleoCausado){
        this.subsidioEmpleoCausado.set(subsidioEmpleoCausado);
    }

    public float getSubsidioEmpleoEntregado(){
        return subsidioEmpleoEntregado.get();
    }

    public void setSubsidioEmpleoEntregado(float subsidioEmpleoEntregado){
        this.subsidioEmpleoEntregado.set(subsidioEmpleoEntregado);
    }

    public float getIsrDeterminado(){
        return isrDeterminado.get();
    }

    public void setIsrDeterminado(float isrDeterminado){
        this.isrDeterminado.set(isrDeterminado);
    }

    public float getIsrPagar(){
        return isrPagar.get();
    }

    public void setIsrPagar(float isrPagar){
        this.isrPagar.set(isrPagar);
    }

    public float getImss(){
        return imss.get();
    }

    public void setIMSS(float imss){
        this.imss.set(imss);
    }

    public float getNeto(){
        return neto.get();
    }

    public void setNeto(float neto){
        this.neto.set(neto);
    }

    public static LinkedList<SueldosSalarios> getSueldosSalarios(ResultSet rs){
        LinkedList<SueldosSalarios> list = new LinkedList<SueldosSalarios>();
        try{
            while(rs.next()) {
                SueldosSalarios sueldosSalarios = new SueldosSalarios();
                sueldosSalarios.setId(rs.getInt("id"));
                sueldosSalarios.setEjercicio(rs.getInt("ejercicio"));
                sueldosSalarios.setDiasPagados(rs.getInt("n_dias_pagados"));
                sueldosSalarios.setDiasVacaciones(rs.getInt("n_dias_vacaciones"));
                sueldosSalarios.setSalarioDiario(rs.getFloat("sd"));
                sueldosSalarios.setSalarioDiarioIntegrado(rs.getFloat("sdi"));
                sueldosSalarios.setSueldosSalarios(rs.getFloat("sueldos_salarios"));
                sueldosSalarios.setSubsidioEmpleoCausado(rs.getFloat("subsidio_empleo_causado"));
                sueldosSalarios.setSubsidioEmpleoEntregado(rs.getFloat("subsidio_empleo_entregado"));
                sueldosSalarios.setIsrDeterminado(rs.getFloat("isr_determinado"));
                sueldosSalarios.setIsrPagar(rs.getFloat("isr_pagar"));
                sueldosSalarios.setIMSS(rs.getFloat("imss"));
                sueldosSalarios.setNeto(rs.getFloat("neto"));
                list.add(sueldosSalarios);
            }
        }catch(SQLException e){
            e.printStackTrace();
            System.exit(1);
        }
        return list;
    }

    public int save(){
        String sql = "INSERT INTO sueldos_salarios "+
            "(ejercicio, n_dias_pagados, n_dias_vacaciones, sd, sdi, sueldos_salarios, subsidio_empleo_causado, "+
            "subsidio_empleo_entregado, isr_determinado, isr_pagar, imss, neto) "+
            "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(getEjercicio());
        params.add(getDiasPagados());
        params.add(getDiasVacaciones());
        params.add(getSalarioDiario());
        params.add(getSalarioDiarioIntegrado());
        params.add(getSueldosSalarios());
        params.add(getSubsidioEmpleoCausado());
        params.add(getSubsidioEmpleoEntregado());
        params.add(getIsrDeterminado());
        params.add(getIsrPagar());
        params.add(getImss());
        params.add(getNeto());
        DBHandler dbHandler = new DBHandler();
        dbHandler.prepareStatement(sql, params);
        int result = dbHandler.executeUpdate();
        dbHandler.close();
        return result;
    }
}
