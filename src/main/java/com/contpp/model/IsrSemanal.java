package com.contpp.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

public class IsrSemanal {
    private int id;
    private float limiteInferior;
    private float limiteSuperior;
    private float cuotaFija;
    private float porcentajeExcedente;

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public float getLimiteInferior(){
        return limiteInferior;
    }

    public void setLimiteInferior(float limiteInferior){
        this.limiteInferior = limiteInferior;
    }

    public float getLimiteSuperior(){
        return limiteSuperior;
    }

    public void setLimiteSuperior(float limiteSuperior){
        this.limiteSuperior = limiteSuperior;
    }

    public float getCuotaFija(){
        return cuotaFija;
    }

    public void setCuotaFija(float cuotaFija){
        this.cuotaFija = cuotaFija;
    }

    public float getPorcentajeExcedente(){
        return porcentajeExcedente;
    }

    public void setPorcentajeExcedente(float porcentajeExcedente){
        this.porcentajeExcedente = porcentajeExcedente;
    }

    public static LinkedList<IsrSemanal> getIsrSemanal(ResultSet rs){
        LinkedList<IsrSemanal> list = new LinkedList<IsrSemanal>();
        try{
            while(rs.next()) {
                IsrSemanal isrSemanal = new IsrSemanal();
                isrSemanal.setId(rs.getInt("id"));
                isrSemanal.setLimiteInferior(rs.getFloat("limite_inferior"));
                isrSemanal.setLimiteSuperior(rs.getFloat("limite_superior"));
                isrSemanal.setCuotaFija(rs.getFloat("cuota_fija"));
                isrSemanal.setPorcentajeExcedente(rs.getFloat("porcentaje_excedente"));
                list.add(isrSemanal);
            }
        }catch(SQLException e){
            e.printStackTrace();
            System.exit(1);
        }
        return list;
    }
}
