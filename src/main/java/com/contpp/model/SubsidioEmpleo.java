package com.contpp.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

public class SubsidioEmpleo {
    private int id;
    private float limiteInferior;
    private float limiteSuperior;
    private float cuotaFija;

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public float getLimiteInferior(){
        return limiteInferior;
    }

    public void setLimiteInferior(float limiteInferior){
        this.limiteInferior = limiteInferior;
    }

    public float getLimiteSuperior(){
        return limiteSuperior;
    }

    public void setLimiteSuperior(float limiteSuperior){
        this.limiteSuperior = limiteSuperior;
    }

    public float getCuotaFija(){
        return cuotaFija;
    }

    public void setCuotaFija(float cuotaFija){
        this.cuotaFija = cuotaFija;
    }

    public static LinkedList<SubsidioEmpleo> getSubsidioEmpleo(ResultSet rs){
        LinkedList<SubsidioEmpleo> list = new LinkedList<SubsidioEmpleo>();
        try{
            while(rs.next()) {
                SubsidioEmpleo subsidioEmpleo = new SubsidioEmpleo();
                subsidioEmpleo.setId(rs.getInt("id"));
                subsidioEmpleo.setLimiteInferior(rs.getFloat("limite_inferior"));
                subsidioEmpleo.setLimiteSuperior(rs.getFloat("limite_superior"));
                subsidioEmpleo.setCuotaFija(rs.getFloat("cuota_fija"));
                list.add(subsidioEmpleo);
            }
        }catch(SQLException e){
            e.printStackTrace();
            System.exit(1);
        }
        return list;
    }
}
