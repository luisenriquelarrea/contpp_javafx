module com.contpp {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;

    opens com.contpp.controller to javafx.fxml;
    opens com.contpp.model to javafx.fxml;

    exports com.contpp;
    exports com.contpp.model;
}
